package http_router

import "net/http"

func (me *Router) GET(u string, method http.HandlerFunc) {
	me.route.Set_route(u, method, http.MethodGet)
}

func (me *Router) HEAD(u string, method http.HandlerFunc) {
	me.route.Set_route(u, method, http.MethodHead)
}

func (me *Router) POST(u string, method http.HandlerFunc) {
	me.route.Set_route(u, method, http.MethodPost)
}

func (me *Router) PUT(u string, method http.HandlerFunc) {
	me.route.Set_route(u, method, http.MethodPut)
}

func (me *Router) PATCH(u string, method http.HandlerFunc) {
	me.route.Set_route(u, method, http.MethodPatch)
}

func (me *Router) DELETE(u string, method http.HandlerFunc) {
	me.route.Set_route(u, method, http.MethodDelete)
}

func (me *Router) CONNECT(u string, method http.HandlerFunc) {
	me.route.Set_route(u, method, http.MethodConnect)
}

func (me *Router) OPTIONS(u string, method http.HandlerFunc) {
	me.route.Set_route(u, method, http.MethodOptions)
}

func (me *Router) TRACE(u string, method http.HandlerFunc) {
	me.route.Set_route(u, method, http.MethodTrace)
}

func (me *Router) NOT_FOUND(method http.HandlerFunc) {
	me.not_found = method
}
