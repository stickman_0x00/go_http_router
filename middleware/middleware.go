package middleware

import "net/http"

type Middleware_func func(next http.Handler) http.Handler

type Middleware []Middleware_func

// Return arg handler wrapped in the middleware.
func (me Middleware) Wrap(h http.Handler) http.Handler {
	// Loop the rest of the middlewares.
	for i := len(me) - 1; i > -1; i-- {
		h = me[i](h)
	}

	return h
}
