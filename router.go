package http_router

import (
	"log"
	"net/http"
	"net/url"

	"gitlab.com/stickman_0x00/go_http_router/middleware"
	"gitlab.com/stickman_0x00/go_http_router/routes"
)

type Router struct {
	route      routes.Route
	middleware middleware.Middleware
	not_found  http.Handler
}

func New() *Router {
	return &Router{
		route:      routes.New("/"),
		middleware: middleware.Middleware{},
		not_found:  http.NotFoundHandler(),
	}
}

// Add middleware to every request. (global)
func (me *Router) Middleware(h middleware.Middleware_func) {
	me.middleware = append(me.middleware, h)
}

func (me *Router) Middleware_route(u string, h middleware.Middleware_func) {
	me.route.Middleware_route(u, h)
}

// ServeHTTP is called when a http request is received.
func (me *Router) ServeHTTP(response http.ResponseWriter, request *http.Request) {
	// Get path unescaped
	path_unescape, err := url.PathUnescape(request.URL.Path)
	if err != nil {
		log.Println("report ERR:", request.URL.RequestURI(), err)
		me.serve(me.not_found, response, request)
		return
	}

	// Get method from route.
	method := me.route.Get_method(path_unescape, request)
	if method == nil {
		// No method found.
		method = me.not_found
	}

	me.serve(method, response, request)
}

func (me *Router) serve(method http.Handler, response http.ResponseWriter, request *http.Request) {
	// Wrap handler around global middleware.
	h := me.middleware.Wrap(method)

	// Handle request
	h.ServeHTTP(response, request)
}

func (me Router) String() string {
	return me.route.String()
}
