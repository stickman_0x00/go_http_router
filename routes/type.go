package routes

type route_type uint8

func (me route_type) String() string {
	return [...]string{
		"END",
		"STATIC",
		"ARGUMENT",
		"FILES",
	}[me]
}

const (
	ROUTE_END route_type = iota
	ROUTE_STATIC
	ROUTE_ARGUMENT
	ROUTE_FILES
)
