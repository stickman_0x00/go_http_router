package routes

import (
	"fmt"
	"net/http"

	"gitlab.com/stickman_0x00/go_http_router/middleware"
)

type route_static struct {
	part       string
	sub_routes routes
	methods    http_methods
	middleware middleware.Middleware
}

func (me route_static) Get_sub_route(url_part string, _ Arguments) *Route {
	if route, exist := me.sub_routes[url_part]; exist {
		return route
	}

	return nil
}

func (me *route_static) Set_sub_route(url_part string) *Route {
	route := New(url_part)
	me.sub_routes[url_part] = &route

	return &route
}

func (me *route_static) Set_method(method http.HandlerFunc, http_method string) {
	me.methods[http_method] = method
}

func (me *route_static) Get_method(http_method string) http.Handler {
	if handler, exist := me.methods[http_method]; exist {
		// Before returning method wrap it in the middleware.
		return me.middleware.Wrap(handler)
	}

	return nil
}

func (me *route_static) Set_middleware(h middleware.Middleware_func) {
	me.middleware = append(me.middleware, h)
}

func (me route_static) String(level int) string {
	s := "|"
	for i := 0; i < level; i++ {
		s += "----|"
	}

	s += fmt.Sprintf("-%s(", me.part)

	s += me.methods.String()

	s += ") - STATIC\n"

	for _, sub := range me.sub_routes {
		s += sub.i_route.String(level + 1)
	}

	return s
}
