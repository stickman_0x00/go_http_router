package routes

import (
	"fmt"
	"net/http"

	"gitlab.com/stickman_0x00/go_http_router/middleware"
)

type route_files struct {
	part       string
	middleware middleware.Middleware
	method     http.HandlerFunc
}

func (me route_files) Get_sub_route(_ string, _ Arguments) *Route {
	return &Route{&me}
}

func (me *route_files) Set_sub_route(_ string) *Route {
	return &Route{me}
}

func (me *route_files) Set_method(method http.HandlerFunc, _ string) {
	me.method = method
}

func (me route_files) Get_method(http_method string) http.Handler {
	return me.middleware.Wrap(me.method)
}

func (me *route_files) Set_middleware(h middleware.Middleware_func) {
	me.middleware = append(me.middleware, h)
}

func (me route_files) String(level int) string {
	s := "|"
	for i := 0; i < level; i++ {
		s += "----|"
	}

	s += fmt.Sprintf("-%s", me.part)

	s += " - FILES\n"

	return s
}
