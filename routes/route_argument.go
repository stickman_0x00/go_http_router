package routes

import (
	"fmt"
	"net/http"

	"gitlab.com/stickman_0x00/go_http_router/middleware"
)

type Arguments map[string]string

type arg_key string

var Arg_key arg_key = "arguments"

type route_argument struct {
	part       string
	arg_name   string
	sub_route  *Route
	methods    http_methods
	middleware middleware.Middleware
}

// Add argument to the args (only matter in case of request)
func (me route_argument) Get_sub_route(url_part string, args Arguments) *Route {
	args[me.arg_name] = url_part

	return me.sub_route
}

func (me *route_argument) Set_sub_route(url_part string) *Route {
	route := New(url_part)
	me.sub_route = &route
	return me.sub_route
}

func (me *route_argument) Set_method(method http.HandlerFunc, http_method string) {
	me.methods[http_method] = method
}

func (me route_argument) Get_method(http_method string) http.Handler {
	if handler, exist := me.methods[http_method]; exist {
		// Before returning method wrap it in the middleware.
		return me.middleware.Wrap(handler)
	}

	return nil
}

func (me *route_argument) Set_middleware(h middleware.Middleware_func) {
	me.middleware = append(me.middleware, h)
}

func (me route_argument) String(level int) string {
	s := "|"
	for i := 0; i < level; i++ {
		s += "----|"
	}

	s += fmt.Sprintf("-%s(", me.part)

	s += me.methods.String()

	s += ") - ARGUMENT\n"

	s += me.sub_route.i_route.String(level + 1)

	return s
}
