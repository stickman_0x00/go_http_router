package routes

import (
	"fmt"
	"net/http"

	"gitlab.com/stickman_0x00/go_http_router/middleware"
)

type route_end struct {
	part       string
	methods    http_methods
	middleware middleware.Middleware
}

func (me route_end) Get_sub_route(_ string, _ Arguments) *Route {
	return nil
}

func (me route_end) Set_sub_route(_ string) *Route {
	return nil
}

func (me *route_end) Set_method(method http.HandlerFunc, http_method string) {
	me.methods[http_method] = method
}

func (me route_end) Get_method(http_method string) http.Handler {
	if handler, exist := me.methods[http_method]; exist {
		// Before returning method wrap it in the middleware.
		return me.middleware.Wrap(handler)
	}

	return nil
}

func (me *route_end) Set_middleware(h middleware.Middleware_func) {
	me.middleware = append(me.middleware, h)
}

func (me route_end) String(level int) string {
	s := "|"
	for i := 0; i < level; i++ {
		s += "----|"
	}

	s += fmt.Sprintf("-%s(", me.part)

	s += me.methods.String()

	s += ") - END\n"

	return s
}
