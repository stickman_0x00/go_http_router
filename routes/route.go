package routes

import (
	"context"
	"log"
	"net/http"

	"gitlab.com/stickman_0x00/go_http_router/middleware"
	"gitlab.com/stickman_0x00/go_http_router/utils"
)

type http_methods map[string]http.HandlerFunc

func (me http_methods) String() string {
	s := ""
	count := 0
	for method := range me {
		if count != 0 {
			s += "|"
		}
		s += method
		count++
	}

	return s
}

type i_route interface {
	// Returns nil if sub route doesn't exist.
	Get_sub_route(url_part string, args Arguments) *Route
	// If returns nil sub route was not created.
	Set_sub_route(url_part string) *Route

	// Returns nil if method not defined.
	// Wrap middleware.
	Get_method(http_method string) http.Handler
	Set_method(method http.HandlerFunc, http_method string)

	// Add middleware
	Set_middleware(h middleware.Middleware_func)

	String(level int) string
}

type Route struct {
	i_route
}

type routes map[string]*Route

func New(url_part string) Route {
	return Route{
		&route_end{
			part:       url_part,
			methods:    make(http_methods),
			middleware: middleware.Middleware{},
		},
	}
}

func (me *Route) get_route(path_unescape string, args Arguments) *Route {
	// Get url parts.
	url_parts, _ := utils.Split_url(path_unescape)

	route := me
	for _, url_part := range url_parts {
		// Pass request to receive arguments on context.
		if temp := route.Get_sub_route(url_part, args); temp != nil {
			route = temp
			continue
		}

		return nil
	}

	return route
}

func (me *Route) Get_method(path_unescape string, request *http.Request) http.Handler {
	args := Arguments{}

	route := me.get_route(path_unescape, args)
	if route == nil {
		return nil
	}

	ctx := context.WithValue(request.Context(), Arg_key, args)
	*request = *request.WithContext(ctx)

	return route.i_route.Get_method(request.Method)
}

// Create the necessary sub routes and add the method to route.
func (me *Route) Set_route(u string, method http.HandlerFunc, http_method string) {
	// Split url into parts.
	url_parts, _ := utils.Split_url(u)

	// Variable of route we are working in the moment.
	route := me
	for _, url_part := range url_parts {
		// Check if sub_route already exists.
		if temp := route.Get_sub_route(url_part, Arguments{}); temp != nil {
			// Save the address of route found.
			route = temp

			continue // Go to the next url_part.
		}

		// Redefine in case it's a route_end.
		route.redefine(url_part)

		// Create new route.
		route = route.Set_sub_route(url_part)
	}

	// Set method in the last route found/created.
	route.Set_method(method, http_method)
}

// If route is type route_end redefine it.
// Receives the next url_part of the next route.
func (me *Route) redefine(url_part string) {
	// Only continue if its type route_end.
	end_route, is_end_route := me.i_route.(*route_end)
	if !is_end_route {
		return
	}

	// Redefine route.
	// IMPORTANT: We need to check the NEXT url_part for
	// the redefinition of the current route.

	// Check if ROUTE_FILES
	if url_part == "*" {
		me.i_route = &route_files{
			part:       end_route.part,
			middleware: end_route.middleware,
		}
		return
	}

	// Get argument name from url_part.
	arg_name, exist := utils.Get_argument_name(url_part)
	if exist {
		// If next url_part is an argument.
		me.i_route = &route_argument{
			part:       end_route.part,
			arg_name:   arg_name,
			sub_route:  nil,
			methods:    end_route.methods,
			middleware: end_route.middleware,
		}
		return
	}

	// If next url_part is not an argument.
	me.i_route = &route_static{
		part:       end_route.part,
		sub_routes: make(routes),
		methods:    end_route.methods,
		middleware: end_route.middleware,
	}

}

// Add middleware to a specific route. (specific)
func (me *Route) Middleware_route(u string, h middleware.Middleware_func) {
	// Get route.
	route := me.get_route(u, Arguments{})
	if route == nil {
		// No route found.
		log.Printf("Route \"%s\" doesn't exist, set route first.", u)
		return
	}

	// Add middleware
	route.Set_middleware(h)
}

func (me Route) String() string {
	s := me.i_route.String(0)
	return s[:len(s)-1]
}
