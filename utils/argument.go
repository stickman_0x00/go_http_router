package utils

import "regexp"

// Regex to catch arguments name.
var arg_regex = func() *regexp.Regexp {
	return regexp.MustCompile(`{(\w*)}`)
}

// If url_part is an argument pattern then returns the name of argument.
// Returns empty string if url_part is not an argmunent.
// Example: {arg_name} returns arg_name.
func Get_argument_name(url_part string) (string, bool) {
	if arg := arg_regex().FindStringSubmatch(url_part); len(arg) == 2 {
		return arg[1], true
	}

	return "", false
}
