package utils

import "strings"

func Split_url(u string) ([]string, int) {
	var url_parts []string

	for _, url_part := range strings.Split(u, "/") {
		if url_part == "" {
			continue
		}

		url_parts = append(url_parts, url_part)
	}

	return url_parts, len(url_parts)
}
