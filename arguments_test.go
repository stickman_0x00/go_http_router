package http_router_test

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	http_router "gitlab.com/stickman_0x00/go_http_router"
)

type test_args struct {
	url             string
	expected_status int
	expected_args   map[string]string
}

func Test_Args(t *testing.T) {
	table_test := []test_args{
		{"/test", http.StatusNotFound, nil},
		{"/test/potato_1", http.StatusOK, map[string]string{"var_1": "potato_1"}},
		{"/test/potato_1/test/", http.StatusNotFound, nil},
		{"/test/potato_1/test/potato_2", http.StatusOK, map[string]string{"var_1": "potato_1", "var_2": "potato_2"}},
	}

	r := http_router.New()
	r.GET("/test/{var_1}", helper_handler)
	r.GET("/test/{var_1}/test/{var_2}", helper_handler)

	for _, test := range table_test {
		request, err := http.NewRequest(http.MethodGet, test.url, nil)
		if err != nil {
			t.Fatalf("could not create request: %v", err)
		}
		response := httptest.NewRecorder()

		r.ServeHTTP(response, request)

		if response.Result().StatusCode != test.expected_status {
			fmt.Println(r)
			t.Fatalf("Request failed: %s, expected:%d received:%d.\n",
				test.url,
				test.expected_status,
				response.Result().StatusCode)
		} else if test.expected_status == http.StatusNotFound {
			continue
		}

		args := http_router.Args(request)
		for key, arg := range args {
			if arg != test.expected_args[key] {
				t.Fatalf("Incorrect arguments: %s, expected:%v received:%v.\n",
					test.url,
					test.expected_args,
					args)
			}
		}

	}

}
