package http_router

import (
	"net/http"

	"gitlab.com/stickman_0x00/go_http_router/routes"
)

func Args(request *http.Request) map[string]string {
	args, ok := request.Context().Value(routes.Arg_key).(routes.Arguments)
	if !ok {
		return nil
	}

	return args
}
