package http_router_test

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	http_router "gitlab.com/stickman_0x00/go_http_router"
)

type test_request struct {
	url             string
	method          string
	expected_status int
}

func helper_handler(response http.ResponseWriter, request *http.Request) {
	fmt.Fprintln(response, "Works!")
}

func TestNewRouter(t *testing.T) {
	table_test := []test_request{
		{"/", http.MethodGet, http.StatusNotFound},
	}

	r := http_router.New()
	for _, test := range table_test {
		request, err := http.NewRequest(test.method, test.url, nil)
		if err != nil {
			t.Fatalf("could not create request: %v", err)
		}
		response := httptest.NewRecorder()

		r.ServeHTTP(response, request)

		if response.Result().StatusCode != test.expected_status {
			fmt.Println(r)
			t.Fatalf("Request failed: %s %s, expected:%d received:%d.\n",
				test.url,
				test.method,
				test.expected_status,
				response.Result().StatusCode)
		}
	}

	table_test = []test_request{
		{"/", http.MethodGet, http.StatusOK},
		{"/test", http.MethodGet, http.StatusNotFound},
	}
	r.GET("/", helper_handler)
	for _, test := range table_test {
		request, err := http.NewRequest(test.method, test.url, nil)
		if err != nil {
			t.Fatalf("could not create request: %v", err)
		}
		response := httptest.NewRecorder()

		r.ServeHTTP(response, request)

		if response.Result().StatusCode != test.expected_status {
			fmt.Println(r)
			t.Fatalf("Request failed: %s %s, expected:%d received:%d.\n",
				test.url,
				test.method,
				test.expected_status,
				response.Result().StatusCode)
		}
	}
}

func TestSimple(t *testing.T) {
	table_test := []test_request{
		{"/", http.MethodGet, http.StatusNotFound},
		{"/test", http.MethodGet, http.StatusOK},
		{"/potato/", http.MethodGet, http.StatusNotFound},
		{"/potato/test", http.MethodGet, http.StatusOK},
		{"/potato/test/potato", http.MethodGet, http.StatusOK},
		{"/potata/", http.MethodGet, http.StatusNotFound},
		{"/potata/test/", http.MethodGet, http.StatusNotFound},
		{"/potata/test/potato", http.MethodGet, http.StatusOK},
		{"/potata/tests/potato", http.MethodPost, http.StatusNotFound},
		{"/potata/tests/method", http.MethodGet, http.StatusOK},
		{"/potata/tests/method", http.MethodPost, http.StatusOK},
	}

	r := http_router.New()
	r.GET("/test", helper_handler)
	r.GET("/potato/test", helper_handler)
	r.GET("/potato/test/potato", helper_handler)
	r.GET("/potata/test/potato", helper_handler)
	r.GET("/potata/tests/method", helper_handler)
	r.POST("/potata/tests/method", helper_handler)
	for _, test := range table_test {
		request, err := http.NewRequest(test.method, test.url, nil)
		if err != nil {
			t.Fatalf("could not create request: %v", err)
		}
		response := httptest.NewRecorder()

		r.ServeHTTP(response, request)

		if response.Result().StatusCode != test.expected_status {
			fmt.Println(r)
			t.Fatalf("Request failed: %s %s, expected:%d received:%d.\n",
				test.url,
				test.method,
				test.expected_status,
				response.Result().StatusCode)
		}
	}
}
