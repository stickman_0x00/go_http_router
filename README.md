# HTTP Router

# How to use

In the end of declaring the routes use to visualize it.

```go
fmt.Println(r)
```

## Route End and Route Static

Route end is when there is no more url path. Route static is when the next url part is defined.

Example of one route static "/", "/a"(not method defined).

Example of route end "/b", "/a/b".

```go
func index(response http.ResponseWriter, request *http.Request) {}
func main() {
	r := http_router.New()
	r.GET("/", index)
	r.GET("/a/b", index)
	r.GET("/b", index)
}
```

## Route Static and Route var

Example of route static "/", "/a"(not method).

Example of route var "/a/{var_1}", "/a/{var_1}/{var_2}". Example to get the variable value too.

```go
func index(response http.ResponseWriter, request *http.Request) {
	args := http_router.Args(request)
	fmt.Fprintln(response, request.URL.RequestURI(), args["var_1"])
	fmt.Fprintln(response, request.URL.RequestURI(), args["var_2"])
}
func main() {
	r := http_router.New()
	r.GET("/", index)
	r.GET("/a/{var_1}", index)
	r.GET("/a/{var_1}/{var_2}", index)
}
```

## Route Static and Route files

Example of route static "/"(not method).

Example of route files "/css/\*".

```go
func css(response http.ResponseWriter, request *http.Request) {
	http.StripPrefix("/css/", http.FileServer(http.Dir("./static/css"))).ServeHTTP(response, request)
}
func main() {
	r := http_router.New()
	r.GET("/css/*", css)
}
```

# Test program

```go
package main

import (
	"fmt"
	"log"
	"net/http"

	http_router "gitlab.com/stickman_0x00/go_http_router"
)

func index(response http.ResponseWriter, request *http.Request) {
	args := http_router.Args(request)
	if a, exist := args["var"]; exist {
		fmt.Fprintln(response, "var:", a)

	}
	if a, exist := args["var2"]; exist {
		fmt.Fprintln(response, "var2:", a)

	}
	if a, exist := args["var3"]; exist {
		fmt.Fprintln(response, "var3:", a)

	}
	fmt.Fprintln(response, request.URL.RequestURI(), args)
}

func log_info(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		timer := time.Now()
		log.Println("1a")
		next.ServeHTTP(w, r)
		log.Println("1a")
		log.Println(r.URL, r.Method, time.Since(timer))
	})
}

func b1(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Println("1b")
		next.ServeHTTP(w, r)
		log.Println("1b")
	})
}

func a(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Println("a")
		next.ServeHTTP(w, r)
		log.Println("a")
	})
}

func b(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Println("b")
		next.ServeHTTP(w, r)
		log.Println("b")
	})
}

func css(response http.ResponseWriter, request *http.Request) {
	http.StripPrefix("/css/", http.FileServer(http.Dir("./css"))).ServeHTTP(response, request)
}

func main() {
	r := http_router.New()
	r.GET("/", index)
	r.GET("/ola", index)
	r.GET("/ola/a", index)
	r.GET("/ol/b", index)
	r.GET("/ol/b/e", index)
	r.GET("/ol/b/c/d/e", index)
	r.POST("/", index)
	r.POST("/ola", index)
	r.GET("/asd/{var}", index)
	r.GET("/asd/{var}/a", index)
	r.GET("/asd/{var}/a/{var2}/a", index)
	r.GET("/opa/{var}/a/{var3}/{var2}", index)
	r.GET("/opa/{var}/a/{var3}", index)
	r.GET("/asd/b", index)
	r.GET("/asd/", index)
	r.GET("/css/*", css)

	r.Middleware(log_info)
	r.Middleware_route("/ol/b", a)
	r.Middleware_route("/ol/b", b)
	r.Middleware_route("/ol/b/e", b)
	r.Middleware_route("/asd/{var}", b)
	r.Middleware(b1)

	fmt.Println(r)
	log.Fatalln(http.ListenAndServe("localhost:8000", r))
}
```
